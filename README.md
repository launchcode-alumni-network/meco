# MECO
The MECO Program was created by LaunchCode Alum's Mike Tully and Aleesha Dawson as a way to continue the conversation and support individuals on their journey into software. 

MECO, a term borrowed from space exploration and launch sequencing, stands for "Main Engine Cut-Off." After the initial journey with LaunchCode is over, many alums find themselves trying to figure out, "What's next?" Our mission is to help answer that question.